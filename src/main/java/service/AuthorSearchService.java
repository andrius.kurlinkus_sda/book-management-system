package service;

import entity.Author;
import repository.AuthorRepository;

import java.util.List;
import java.util.stream.Collectors;

public class AuthorSearchService {
    private AuthorRepository authorRepository;

    // dependency injection via constructor
    public AuthorSearchService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public List<Author> findByName(String name){
        return authorRepository.getAll().stream()
                .filter(author -> author.getFirstName().contains(name))
                .collect(Collectors.toList());
    }
}
