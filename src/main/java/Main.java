import entity.Author;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import repository.AuthorRepository;
import service.AuthorSearchService;
import util.HibernateUtil;

public class Main extends Application {
    private static SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    private static AuthorRepository authorRepository = new AuthorRepository(sessionFactory);
    private static AuthorSearchService ass = new AuthorSearchService(authorRepository);

    private final TextField searchField = new TextField("Enter the name of the author to find");
    private final TableView<Author> authorTable = new TableView<>();
    private final ObservableList<Author> authorList = FXCollections.observableArrayList();
    private final Label authorCountLabel = new Label();
    private Scene scene;

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Scene getScene() {
        return this.scene;
    }

    public static void main(String[] args) {
        //addInitialData();
        launch(args);
    }

    public static void addInitialData() {
        // ... inject some data to make the application self sufficient
        authorRepository.create(new Author("Dan", "Brown"));
        authorRepository.create(new Author("Mario", "Puzo"));
    }

    @Override
    public void start(Stage stage) {

        stage.setTitle("Book Management System");
        stage.setWidth(600);
        stage.setHeight(500);

        setTableAppearance();

        fillTableObservableListWithSampleData();
        authorTable.setItems(authorList);

        TableColumn<Author, Integer> colId = new TableColumn<>("ID");
        colId.setCellValueFactory(new PropertyValueFactory<>("authorId"));

        TableColumn<Author, String> colName = new TableColumn<>("Name");
        colName.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Author, String> colSurname = new TableColumn<>("Surname");
        colSurname.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        authorTable.getColumns().addAll(colId, colName, colSurname);
        addButtonToTable();


        TabPane tabPane = new TabPane();
        VBox parentRoot = new VBox(tabPane);
        Tab author = new Tab("Authors");
        Tab book = new Tab("Books");
        Tab review = new Tab("Reviews");
        tabPane.getTabs().addAll(author,book,review);
        VBox root = new VBox();
        author.setContent(root);
        root.setId("id-root");
        HBox searchBar = new HBox();
        searchBar.setId("id-search-bar");
        HBox.setHgrow(searchField, Priority.ALWAYS);
        searchField.setId("id-search-field");
        searchBar.getChildren().add(searchField);

        Button searchButton = new Button("Search");
        searchButton.setId("id-search-button");
        searchButton.setOnAction(actionEvent ->
                authorList.setAll(ass.findByName(searchField.getText())));
        searchBar.getChildren().add(searchButton);
        root.getChildren().add(searchBar);
        root.getChildren().add(authorTable);
        root.getChildren().add(authorCountLabel);

        HBox newAuthorCreationArea = new HBox();
        TextField newName = new TextField();
        TextField newSurname = new TextField();
        Button createNewButton = new Button("Create");
        createNewButton.setOnAction(actionEvent -> {
            authorRepository.create(new Author(newName.getText(), newSurname.getText()));
            authorList.setAll(authorRepository.getAll());
        });

        newAuthorCreationArea.getChildren().addAll(newName, newSurname, createNewButton);
        root.getChildren().add(newAuthorCreationArea);

        scene = new Scene(parentRoot);
        stage.setScene(scene);
        stage.show();
    }

    private void setTableAppearance() {
        authorTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        authorTable.setPrefWidth(600);
        authorTable.setPrefHeight(300);
    }

    private void fillTableObservableListWithSampleData() {
        authorList.setAll(authorRepository.getAll());
        authorCountLabel.setText("Total authors: " + authorList.size());
    }

    private void addButtonToTable() {
        TableColumn<Author, Void> colBtn = new TableColumn("Button Column");
        Callback<TableColumn<Author, Void>, TableCell<Author, Void>> cellFactory =
                new Callback<TableColumn<Author, Void>, TableCell<Author, Void>>() {
                    @Override
                    public TableCell<Author, Void> call(final TableColumn<Author, Void> param) {
                        final TableCell<Author, Void> cell = new TableCell<Author, Void>() {
                            private final Button btn = new Button("Delete item");

                            {
                                btn.setOnAction((ActionEvent event) -> {
                                    Author author = getTableView().getItems().get(getIndex());
                                    System.out.println("clicked on: " + author);
                                    Session session = sessionFactory.openSession();
                                    Transaction transaction = session.beginTransaction();
                                    Author authorToDelete = session.load(Author.class, author.getAuthorId());
                                    session.delete(authorToDelete);
                                    transaction.commit();
                                    session.close();
                                    fillTableObservableListWithSampleData();
                                });
                            }

                            @Override
                            public void updateItem(Void item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                } else {
                                    setGraphic(btn);
                                }
                            }
                        };
                        return cell;
                    }
                };
        colBtn.setCellFactory(cellFactory);
        authorTable.getColumns().add(colBtn);
    }
}
